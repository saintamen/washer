package com.vickl.sda.nordea;

import java.awt.EventQueue;

import javax.swing.JFrame;
import java.awt.GridLayout;
import javax.swing.JPanel;

public class PralniaOkienko {

	private JFrame frame;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					PralniaOkienko window = new PralniaOkienko();
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public PralniaOkienko() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frame = new JFrame();
		frame.setBounds(100, 100, 474, 338);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(new GridLayout(1, 0, 0, 0));
		
		JPanel panel = new PralkaPanel();
		frame.getContentPane().add(panel);
		
		JPanel pane2 = new PralkaPanel();
		frame.getContentPane().add(pane2);
		
		JPanel pane3 = new PralkaPanel();
		frame.getContentPane().add(pane3);
	}

}
