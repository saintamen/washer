package com.vickl.sda.nordea;

import javax.swing.JPanel;
import java.awt.GridBagLayout;

import javax.swing.AbstractButton;
import javax.swing.ButtonGroup;
import javax.swing.JLabel;
import java.awt.GridBagConstraints;
import java.awt.Insets;
import javax.swing.border.LineBorder;

import com.vickl.sda.nordea.programy.Odwirowywanie;
import com.vickl.sda.nordea.programy.PranieDelikatne;
import com.vickl.sda.nordea.programy.PranieEko;
import com.vickl.sda.nordea.programy.PranieNormalne;
import com.vickl.sda.nordea.programy.PranieSzybkie;

import java.awt.Color;
import java.awt.GridLayout;
import javax.swing.JRadioButton;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.util.Enumeration;
import java.awt.event.ActionEvent;
import javax.swing.JCheckBox;

public class PralkaPanel extends JPanel implements IWasherListner {
	
	private Washer washer; 
	private JButton buttonStart; 
	private JButton buttonStop;  
	private ButtonGroup group; 
	Enumeration<AbstractButton> washingButtons; 
	private JCheckBox chckbxWstepne; 
	private JCheckBox chckbxPranie; 
	private JCheckBox chckbxOdwirowywanie; 
	private JLabel statusLabel; 
	
	@Override
	public void phaseFinished(int phase) {
		// przelaczanie checboxow
		//if phase -1 to select wstepne 
		if(phase == 1){
			chckbxWstepne.setSelected(true); 
		}
		if(phase == 2){
			chckbxPranie.setSelected(true); 
		}
		if(phase == 3){
			chckbxOdwirowywanie.setSelected(true); 
		}
	}

	@Override
	public void washerFinished() {
		// TODO Auto-generated method stub
		buttonStart.setEnabled(true);
		buttonStop.setEnabled(false);
		chckbxWstepne.setSelected(false);
		chckbxPranie.setSelected(false);
		chckbxOdwirowywanie.setSelected(false);
	}
	
	@Override
	public void statusUpdate(String message) {
		statusLabel.setText(message);
//		statusLabel.setText("pranie wstepne");
//		statusLabel.setText("pranie wlasciwe");
//		statusLabel.setText("odwirowywanie");
//		statusLabel.setText("pranie zakonczone");
		
	}
	
	/**
	 * Create the panel.
	 */
	public PralkaPanel() {
		washer = new Washer(this); 
		GridBagLayout gridBagLayout = new GridBagLayout();
		gridBagLayout.columnWidths = new int[]{0, 0};
		gridBagLayout.rowHeights = new int[]{0, 0, 0, 0, 0};
		gridBagLayout.columnWeights = new double[]{1.0, Double.MIN_VALUE};
		gridBagLayout.rowWeights = new double[]{0.0, 1.0, 1.0, 1.0, Double.MIN_VALUE};
		setLayout(gridBagLayout);
		
		JLabel lblPralka = new JLabel("Pralka");
		GridBagConstraints gbc_lblPralka = new GridBagConstraints();
		gbc_lblPralka.insets = new Insets(0, 0, 5, 0);
		gbc_lblPralka.gridx = 0;
		gbc_lblPralka.gridy = 0;
		add(lblPralka, gbc_lblPralka);
		
		JPanel panel = new JPanel();
		panel.setBorder(new LineBorder(new Color(0, 0, 0)));
		GridBagConstraints gbc_panel = new GridBagConstraints();
		gbc_panel.insets = new Insets(0, 0, 5, 0);
		gbc_panel.fill = GridBagConstraints.BOTH;
		gbc_panel.gridx = 0;
		gbc_panel.gridy = 1;
		add(panel, gbc_panel);
		panel.setLayout(new GridLayout(0, 1, 0, 0));
		
		
		
		
		JRadioButton radioSzybki = new JRadioButton("Pranie szybkie");
		panel.add(radioSzybki);
		
		radioSzybki.setActionCommand("Pranie szybkie");
		
		JRadioButton radioNormalne = new JRadioButton("Pranie normalne");
		panel.add(radioNormalne);
		radioNormalne.setActionCommand("Pranie normalne");
		
		JRadioButton radioOdwirowywanie = new JRadioButton("Odwirowywanie");
		panel.add(radioOdwirowywanie);
		radioOdwirowywanie.setActionCommand("Odwirowywanie");
		
		JRadioButton radioEko = new JRadioButton("Pranie ekonomiczne ");
		panel.add(radioEko);
		radioEko.setActionCommand("Eko");
		
		JRadioButton radioDelikatne = new JRadioButton("Pranie Delikatne");
		panel.add(radioDelikatne);
		radioDelikatne.setActionCommand("Pranie Delikatne");

		group = new ButtonGroup(); 
		group.add(radioSzybki);
		group.add(radioNormalne); 
		group.add(radioOdwirowywanie); 
		group.add(radioEko); 
		group.add(radioDelikatne);
		
		JPanel panelStan = new JPanel();
		GridBagConstraints gbc_panelStan = new GridBagConstraints();
		gbc_panelStan.insets = new Insets(0, 0, 5, 0);
		gbc_panelStan.fill = GridBagConstraints.BOTH;
		gbc_panelStan.gridx = 0;
		gbc_panelStan.gridy = 2;
		add(panelStan, gbc_panelStan);
		GridBagLayout gbl_panelStan = new GridBagLayout();
		gbl_panelStan.columnWidths = new int[] {0};
		gbl_panelStan.rowHeights = new int[] {0};
		gbl_panelStan.columnWeights = new double[]{0.0};
		gbl_panelStan.rowWeights = new double[]{0.0, 0.0, 0.0, 0.0, 0.0};
		panelStan.setLayout(gbl_panelStan);
		
		JLabel lblStanPrania = new JLabel("Stan prania: ");
		GridBagConstraints gbc_lblStanPrania = new GridBagConstraints();
		gbc_lblStanPrania.anchor = GridBagConstraints.WEST;
		gbc_lblStanPrania.insets = new Insets(0, 0, 0, 5);
		gbc_lblStanPrania.gridx = 0;
		gbc_lblStanPrania.gridy = 1;
		panelStan.add(lblStanPrania, gbc_lblStanPrania);
		
		chckbxWstepne = new JCheckBox("Pranie wst\u0119pne");
		GridBagConstraints gbc_chckbxWstepne = new GridBagConstraints();
		gbc_chckbxWstepne.anchor = GridBagConstraints.NORTHWEST;
		gbc_chckbxWstepne.insets = new Insets(0, 0, 0, 5);
		gbc_chckbxWstepne.gridx = 0;
		gbc_chckbxWstepne.gridy = 2;
		panelStan.add(chckbxWstepne, gbc_chckbxWstepne);
		chckbxWstepne.setEnabled(false);
		
		chckbxPranie = new JCheckBox("Pranie");
		GridBagConstraints gbc_chckbxPranie = new GridBagConstraints();
		gbc_chckbxPranie.anchor = GridBagConstraints.NORTHWEST;
		gbc_chckbxPranie.insets = new Insets(0, 0, 0, 5);
		gbc_chckbxPranie.gridx = 0;
		gbc_chckbxPranie.gridy = 3;
		panelStan.add(chckbxPranie, gbc_chckbxPranie);
		chckbxPranie.setEnabled(false);
		
		chckbxOdwirowywanie = new JCheckBox("Odwirowywanie");
		GridBagConstraints gbc_chckbxOdwirowywanie = new GridBagConstraints();
		gbc_chckbxOdwirowywanie.anchor = GridBagConstraints.NORTHWEST;
		gbc_chckbxOdwirowywanie.gridx = 0;
		gbc_chckbxOdwirowywanie.gridy = 4;
		panelStan.add(chckbxOdwirowywanie, gbc_chckbxOdwirowywanie);
		chckbxOdwirowywanie.setEnabled(false);
		
		JPanel panelStatus = new JPanel();
		GridBagConstraints gbc_panelStatus = new GridBagConstraints();
		gbc_panelStatus.fill = GridBagConstraints.BOTH;
		gbc_panelStatus.gridx = 0;
		gbc_panelStatus.gridy = 3;
		add(panelStatus, gbc_panelStatus);
		GridBagLayout gbl_panelStatus = new GridBagLayout();
		gbl_panelStatus.columnWidths = new int[] {0};
		gbl_panelStatus.rowHeights = new int[] {0};
		gbl_panelStatus.columnWeights = new double[]{0.0};
		gbl_panelStatus.rowWeights = new double[]{0.0, 0.0, 0.0, 0.0};
		panelStatus.setLayout(gbl_panelStatus);
		
		buttonStop = new JButton("Stop");
		buttonStop.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				washer.stop();
				buttonStart.setEnabled(true);
				buttonStop.setEnabled(false);
			}
		});
		
		buttonStart = new JButton("Start");
		buttonStart.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				String selected = group.getSelection().getActionCommand();
				
				switch (selected) {
				case "Pranie szybkie":
					washer.setWybranyProgram(new PranieSzybkie(PralkaPanel.this));
					break;
				case "Odwirowywanie":
					washer.setWybranyProgram(new Odwirowywanie(PralkaPanel.this));
					break;
				case "Eko":	
					washer.setWybranyProgram(new PranieEko(PralkaPanel.this));
					break;
				case "Pranie Delikatne":
					washer.setWybranyProgram(new PranieDelikatne(PralkaPanel.this));
					break;
				case "Pranie normalne":
					washer.setWybranyProgram(new PranieNormalne(PralkaPanel.this));
				default:
					break;
				} 
				
				washer.start();
				buttonStart.setEnabled(false);
				buttonStop.setEnabled(true);
				
			}
		});
		
		statusLabel = new JLabel("status: ");
		GridBagConstraints gbc_statusLabel = new GridBagConstraints();
		gbc_statusLabel.fill = GridBagConstraints.BOTH;
		gbc_statusLabel.anchor = GridBagConstraints.WEST;
		gbc_statusLabel.insets = new Insets(0, 0, 0, 5);
		gbc_statusLabel.gridx = 0;
		gbc_statusLabel.gridy = 1;
		panelStatus.add(statusLabel, gbc_statusLabel);
		GridBagConstraints gbc_buttonStart = new GridBagConstraints();
		gbc_buttonStart.weightx = 1.0;
		gbc_buttonStart.fill = GridBagConstraints.BOTH;
		gbc_buttonStart.anchor = GridBagConstraints.NORTHWEST;
		gbc_buttonStart.insets = new Insets(0, 0, 0, 5);
		gbc_buttonStart.gridx = 0;
		gbc_buttonStart.gridy = 2;
		panelStatus.add(buttonStart, gbc_buttonStart);
		GridBagConstraints gbc_buttonStop = new GridBagConstraints();
		gbc_buttonStop.weightx = 1.0;
		gbc_buttonStop.fill = GridBagConstraints.BOTH;
		gbc_buttonStop.anchor = GridBagConstraints.NORTHWEST;
		gbc_buttonStop.gridx = 0;
		gbc_buttonStop.gridy = 3;
		panelStatus.add(buttonStop, gbc_buttonStop);
		
		// wylaczenie radio buttonow
//		washingButtons = group.getElements();
//        while (washingButtons.hasMoreElements()) {
//            washingButtons.nextElement().setEnabled(false);
//        }
	}

	
}
