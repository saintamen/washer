package com.vickl.sda.nordea;

public interface IWasherListner {
	
	void phaseFinished(int phase); 
	void statusUpdate(String message); 
	void washerFinished();
}
