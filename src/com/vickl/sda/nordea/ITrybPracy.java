package com.vickl.sda.nordea;

public interface ITrybPracy {
	
	void przygotujDoPrania() throws InterruptedException;

	void pranie() throws InterruptedException;

	void czynnosciPoPraniu() throws InterruptedException;
}
