package com.vickl.sda.nordea.programy;

import com.vickl.sda.nordea.ITrybPracy;
import com.vickl.sda.nordea.IWasherListner;

public class Odwirowywanie implements ITrybPracy {

	private IWasherListner panel = null;

	public Odwirowywanie(IWasherListner panel) {
		super();
		this.panel = panel;
	}

	@Override
	public void przygotujDoPrania() {
		// TODO Auto-generated method stub

	}

	@Override
	public void pranie() {
		// TODO Auto-generated method stub

	}

	@Override
	public void czynnosciPoPraniu() {
		try {
			panel.statusUpdate("Sprawdzam przepelnienie");
			Thread.sleep(2000);
		} catch (InterruptedException e) {
			// TODO: handle exception
		}
		try {
			panel.statusUpdate("Rozpoczynam wirowanie");
			panel.statusUpdate("Wirowanie 100obr/min");
			Thread.sleep(500);
			panel.statusUpdate("Wirowanie 200obr/min");
			Thread.sleep(500);
			panel.statusUpdate("Wirowanie 400obr/min");
			Thread.sleep(500);
			panel.statusUpdate("Wirowanie 800obr/min");
			Thread.sleep(1000);
			panel.statusUpdate("Wirowanie 1000obr/min");
			Thread.sleep(1000);
		} catch (InterruptedException e) {
			// TODO: handle exception
		}

	}

}
