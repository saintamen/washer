package com.vickl.sda.nordea.programy;

import com.vickl.sda.nordea.ITrybPracy;
import com.vickl.sda.nordea.IWasherListner;

public class PranieSzybkie implements ITrybPracy {

	private IWasherListner panel = null;

	public PranieSzybkie(IWasherListner panel) {
		super();
		this.panel = panel;
	}

	@Override
	public void przygotujDoPrania() throws InterruptedException {
		panel.statusUpdate("Rozpoczynam pranie");
		Thread.sleep(1000);
		panel.statusUpdate("Nalewam wody do pralki");
		Thread.sleep(1000);
		panel.statusUpdate("Przygotowywanie zako�czone");
		Thread.sleep(1000);
	}

	@Override
	public void pranie() throws InterruptedException {
		panel.statusUpdate("Nalewam wody");
		Thread.sleep(1000);
		panel.statusUpdate("Pranie");
		Thread.sleep(1000);
		panel.statusUpdate("Wypuszczanie wody");
		Thread.sleep(1000);
	}

	@Override
	public void czynnosciPoPraniu() throws InterruptedException {
		panel.statusUpdate("Sprawdzam przepelnienie");
		Thread.sleep(1000);
		panel.statusUpdate("Wirowanie 400obr/min");
		Thread.sleep(500);
		panel.statusUpdate("Wirowanie 800obr/min");
		Thread.sleep(1000);
		panel.statusUpdate("Wirowanie 1000obr/min");
		Thread.sleep(5000);

	}

}
