package com.vickl.sda.nordea.programy;

import com.vickl.sda.nordea.ITrybPracy;
import com.vickl.sda.nordea.IWasherListner;

public class PranieNormalne implements ITrybPracy {

	private IWasherListner panel = null;

	public PranieNormalne(IWasherListner panel) {
		super();
		this.panel = panel;
		
	}

	@Override
	public void przygotujDoPrania() {
		try {
			panel.statusUpdate("Rozpoczynam Pranie");
			Thread.sleep(3000);
		} catch (InterruptedException e) {
			System.out.println("interrupt");
		}
		try {
			panel.statusUpdate("Nalewam wody do pralki");
			Thread.sleep(5000);
		} catch (InterruptedException e) {
			System.out.println("interrupt");
		}
		try {
			panel.statusUpdate("Nalewam wody do pralki");
			Thread.sleep(5000);
		} catch (InterruptedException e) {
			System.out.println("interrupt");
		}
	}

	@Override
	public void pranie() {
		try {
			panel.statusUpdate("Nalewam wody");
			Thread.sleep(3000);
		} catch (InterruptedException e) {
			System.out.println("interrupt");
		}
		try {
			panel.statusUpdate("Pranie");
			Thread.sleep(2000);
		} catch (InterruptedException e) {
			System.out.println("interrupt");
		}
		try {
			panel.statusUpdate("Wypuszczanie wody");
			Thread.sleep(1000);
		} catch (InterruptedException e) {
			System.out.println("interrupt");
		}

	}

	@Override
	public void czynnosciPoPraniu() {
		try {
			panel.statusUpdate("Sprawdzam przepelnienie");
			Thread.sleep(2000);
		} catch (InterruptedException e) {
			System.out.println("interrupt");
		}
		try {
			panel.statusUpdate("Rozpoczynam wirowanie");
			panel.statusUpdate("Wirowanie 100obr/min");
			Thread.sleep(500);
			panel.statusUpdate("Wirowanie 200obr/min");
			Thread.sleep(500);
			panel.statusUpdate("Wirowanie 400obr/min");
			Thread.sleep(500);
			panel.statusUpdate("Wirowanie 800obr/min");
			Thread.sleep(1000);
			panel.statusUpdate("Wirowanie 1000obr/min");
			Thread.sleep(1000);
		} catch (InterruptedException e) {
			System.out.println("interrupt");
		}
	}
}
