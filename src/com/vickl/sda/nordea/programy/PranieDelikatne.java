package com.vickl.sda.nordea.programy;

import com.vickl.sda.nordea.ITrybPracy;
import com.vickl.sda.nordea.IWasherListner;

public class PranieDelikatne implements ITrybPracy {

	private IWasherListner panel = null;

	public PranieDelikatne(IWasherListner panel) {
		super();
		this.panel = panel;
	}

	@Override
	public void przygotujDoPrania() {
		try {
			panel.statusUpdate("Rozpoczynam pranie");
			Thread.sleep(3000);
		} catch (InterruptedException e) {
			// TODO: handle exception
		}
		try {
			panel.statusUpdate("Nalewam wody do pralki");
			Thread.sleep(3000);
		} catch (InterruptedException e) {
			// TODO: handle exception
		}
		try {
			panel.statusUpdate("Przygotowywanie zako�czone");
			Thread.sleep(3000);
		} catch (InterruptedException e) {
			// TODO: handle exception
		}

	}

	@Override
	public void pranie() {
		try {
			panel.statusUpdate("Nalewam wody");
			Thread.sleep(3000);
		} catch (InterruptedException e) {
			// TODO: handle exception
		}
		try {
			panel.statusUpdate("Pranie");
			Thread.sleep(3000);
		} catch (InterruptedException e) {
			// TODO: handle exception
		}
		try {
			panel.statusUpdate("Wypuszczanie wody");
			Thread.sleep(1000);
		} catch (InterruptedException e) {
			// TODO: handle exception
		}

	}

	@Override
	public void czynnosciPoPraniu() {
		try {
			panel.statusUpdate("Sprawdzam przepelnienie");
			Thread.sleep(2000);
		} catch (InterruptedException e) {
			// TODO: handle exception
		}
		try {
			panel.statusUpdate("Rozpoczynam wirowanie");
			panel.statusUpdate("Wirowanie 100obr/min");
			Thread.sleep(500);
			panel.statusUpdate("Wirowanie 200obr/min");
			Thread.sleep(500);
			panel.statusUpdate("Wirowanie 400obr/min");
			Thread.sleep(500);
		} catch (InterruptedException e) {
			// TODO: handle exception
		}
	}

}
