package com.vickl.sda.nordea;

import java.util.concurrent.Phaser;

import com.vickl.sda.nordea.programy.PranieNormalne;

public class Washer implements Runnable {

	private ITrybPracy wybranyProgram = null;
	private Thread watek; 
	private IWasherListner panel = null; 
//	private IWasherListner listner; 
	
	public ITrybPracy getWybranyProgram() {
		return wybranyProgram;
	}

	public void setWybranyProgram(ITrybPracy wybranyProgram) {
		this.wybranyProgram = wybranyProgram;
	}

	public Washer(IWasherListner panel) {
		super();
		this.panel = panel;
	}

//	private ExecutorService watek = Executors.newSingleThreadExecutor();

	@Override
	public void run() {
		try {
			wybranyProgram.przygotujDoPrania();
			//koniec 1 fazy 
			panel.phaseFinished(1);
			wybranyProgram.pranie();
			//koniec 2 fazy 
			panel.phaseFinished(2);
			wybranyProgram.czynnosciPoPraniu();
			//koniec 3 fazy 
			panel.phaseFinished(3);
			Thread.sleep(1000);
		} catch (InterruptedException e) {
			panel.statusUpdate("Pranie przerwane.");
		}
		
		//timer na zamek
		panel.washerFinished();
	}

	public void start(){
		watek = new Thread(this);
		watek.start();
		
		System.out.println("Start");
	}

	public void stop() {
		watek.interrupt();
		System.out.println("Stop");
	}

}
